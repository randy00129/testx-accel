Django==2.2
djangorestframework==3.11.0
gunicorn==20.0.4
psycopg2==2.8.4
psycopg2-binary==2.8.4
